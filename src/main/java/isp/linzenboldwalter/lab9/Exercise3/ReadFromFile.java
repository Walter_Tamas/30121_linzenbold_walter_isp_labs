package isp.linzenboldwalter.lab9.Exercise3;


import java.awt.*;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class ReadFromFile extends JFrame {

    JLabel name;
    JTextField location;
    JTextArea content;
    JButton button;

    ReadFromFile() {
        setTitle("Exercise 3");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(700, 700);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 500;
        int height = 20;


        location = new JTextField();
        location.setBounds(100, 50, width, height);
        name = new JLabel();
        name.setBounds(10, 50, width, height);
        name.setText("File name:");
        content = new JTextArea();
        content.setBounds(10, 100, 590, 400);
        button = new JButton("Show content");
        button.setBounds(10, 520, 150, 20);

        button.addActionListener(new TratareButon());
        add(location);
        add(name);
        add(content);
        add(button);

    }

    public static void main(String[] args) {
        new ReadFromFile();
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {

           content.setText("");
            try{
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader(new File(ReadFromFile.this.location.getText())));
           String s =new String();
            while((s=bf.readLine())!=null)
            {

                content.append(s+"\n");
            }

        }catch(Exception ex)
            {
                System.out.println(ex.getMessage());
            }
        }
    }

}
