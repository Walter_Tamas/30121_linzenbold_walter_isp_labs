package isp.linzenboldwalter.lab9.Exercise5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;

public class UserInterface extends JFrame {

    public static boolean simulate=false;
    JLabel[] station = new JLabel[3];
    JLabel addingTrain, newTrainId, newTrainDestinationLabel, newTrainSegment;
    JTextField newTrainName, newTrainDestination, trainSegment;
    JTextField[][] segment = new JTextField[3][3];
    JButton addTrain,runSim;

    UserInterface(String[] stations, Segment[] trains) {
        setTitle("Train simulator");
        setLocation(0, 0);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init(stations, trains);
        setSize(1000, 1000);
        setVisible(true);

    }

    public void init(String[] stations, Segment[] trains) {
        this.setLayout(new GridLayout(22, 1));
        for (int i = 0; i < 3; i++) {
            station[i] = new JLabel("Station " + stations[i]);
            for (int j = 0; j < 3; j++) {
                segment[i][j] = new JTextField("Segment is empty");
                if (trains[3 * i + j + 1].hasTrain())
                    segment[i][j].setText(trains[3 * i + j + 1].getTrain().name);
            }
        }
        /** Buttons and labels*/
        for (int i = 0; i < 3; i++) {
            add(station[i]);
            for (int j = 0; j < 3; j++)
                add(segment[i][j]);
        }

        addingTrain = new JLabel("New train:");
        newTrainId = new JLabel("Train ID:");
        newTrainName = new JTextField();
        newTrainDestinationLabel = new JLabel("Destination:");
        newTrainDestination = new JTextField();
        newTrainSegment = new JLabel("Current segment:");
        trainSegment = new JTextField();
        add(addingTrain);
        add(newTrainId);
        add(newTrainName);
        add(newTrainDestinationLabel);
        add(newTrainDestination);
        add(newTrainSegment);
        add(trainSegment);
        addTrain=new JButton("Add");
        add(addTrain);
        runSim=new JButton("Run simulation");
        add(runSim);
        runSim.addActionListener(new SimulationButton());
        addTrain.addActionListener(new ButtonEvent());
    }

    class ButtonEvent implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Simulator.trainAdd(newTrainDestination.getText(), newTrainName.getText(), Integer.parseInt(trainSegment.getText()));
            }catch (Exception ex)
            {  showMessageDialog(null, "Invalid informations");}
        }
    }
    class SimulationButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
           simulate=true;
           Simulator.Simulate();
        }
    }

}
