package isp.linzenboldwalter.lab9.Exercise5;

public class Simulator {

    /**
     * @param args
     */
    public static String[] stations={new String("Cluj-Napoca"),new String("Bucuresti"),new String("Satu-Mare")};
    public static Segment[] s={new Segment(0),new Segment(1),new Segment(2),new Segment(3),new Segment(4),new Segment(5),new Segment(6),new Segment(7),new Segment(8),new Segment(9)};
    public static Controler[] c={new Controler("Cluj-Napoca"),new Controler("Bucuresti"),new Controler("Satu-Mare")};
    public static void main(String[] args) {



        c[0].addControlledSegment(s[1]);
        c[0].addControlledSegment(s[2]);
        c[0].addControlledSegment(s[3]);



        c[1].addControlledSegment(s[4]);
        c[1].addControlledSegment(s[5]);
        c[1].addControlledSegment(s[6]);

        //build station Satu-Mare

        c[2].addControlledSegment(s[7]);
        c[2].addControlledSegment(s[8]);
        c[2].addControlledSegment(s[9]);

        //connect the 2 controllers
        c[0].setNeighbourController(c[1]);
        c[0].setNeighbourController(c[2]);
        c[1].setNeighbourController(c[0]);
        c[1].setNeighbourController(c[2]);
        c[2].setNeighbourController(c[0]);
        c[2].setNeighbourController(c[1]);
        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s[1].arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","R-002");
        s[5].arriveTrain(t2);

        Train t3=new Train("Bucuresti","T-003");
        s[7].arriveTrain(t3);

        c[0].displayStationState();
        c[1].displayStationState();
        c[2].displayStationState();
        System.out.println("\nStart train control\n");
        UserInterface form=new UserInterface(stations,s);

        //execute 3 times controller steps

    }
    public static void trainAdd(String destination,String name,int segment ){
        Train t4=new Train(destination,name);
        s[segment].arriveTrain(t4);
        new UserInterface(stations,s);
    }
    public static void Simulate(){
        if(UserInterface.simulate==true) {
            for (int i = 0; i < 3; i++) {
                System.out.println("### Step " + i + " ###");
                c[0].controlStep();
                c[1].controlStep();
                c[2].controlStep();

                System.out.println();

                c[0].displayStationState();
                c[1].displayStationState();
                c[2].displayStationState();

            }
            new UserInterface(stations,s);
        }
    }
}