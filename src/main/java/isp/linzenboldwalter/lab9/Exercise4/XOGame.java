package isp.linzenboldwalter.lab9.Exercise4;

import java.awt.*;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import java.util.*;

import static javax.swing.JOptionPane.showMessageDialog;


public class XOGame extends JFrame {

    JButton[][] playZone = new JButton[3][3];
    JButton reset;
    JLabel player1, player2, turnValue, turnLabel, symbol1, symbol2;
    JTextField name1, name2;
    JTextField win;
    public static int turn;

    XOGame() {
        setTitle("Tic tac toe");
        setLocation(700, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        turn = 0;
        setSize(400, 400);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 60;
        int height = 20;

        playZone[0][0] = new JButton();
        playZone[0][0].setBounds(10, 30, width, height);
        playZone[0][1] = new JButton();
        playZone[0][1].setBounds(80, 30, width, height);
        playZone[0][2] = new JButton();
        playZone[0][2].setBounds(150, 30, width, height);
        playZone[1][0] = new JButton();
        playZone[1][0].setBounds(10, 60, width, height);
        playZone[1][1] = new JButton();
        playZone[1][1].setBounds(80, 60, width, height);
        playZone[1][2] = new JButton();
        playZone[1][2].setBounds(150, 60, width, height);
        playZone[2][0] = new JButton();
        playZone[2][0].setBounds(10, 90, width, height);
        playZone[2][1] = new JButton();
        playZone[2][1].setBounds(80, 90, width, height);
        playZone[2][2] = new JButton();
        playZone[2][2].setBounds(150, 90, width, height);
        player1 = new JLabel();
        player1.setBounds(10, 120, 50, 20);
        player1.setText("Player 1:");
        player2 = new JLabel();
        player2.setBounds(10, 150, 50, 20);
        player2.setText("Player 2:");
        name1 = new JTextField();
        name1.setBounds(70, 120, 100, 20);
        name2 = new JTextField();
        name2.setBounds(70, 150, 100, 20);
        symbol1 = new JLabel();
        symbol1.setBounds(180, 120, 60, height);
        symbol1.setText("uses X");
        symbol2 = new JLabel();
        symbol2.setBounds(180, 150, 60, height);
        symbol2.setText("uses 0");
        win = new JTextField();
        win.setBounds(50, 200, 200, 50);
        turnValue = new JLabel();
        turnValue.setBounds(350, 30, width, height);
        turnValue.setText("0");
        turnLabel = new JLabel();
        turnLabel.setBounds(290, 30, width, height);
        turnLabel.setText("Turn no.:");
        reset = new JButton("Restart");
        reset.setBounds(50, 260, 200, height);

        reset.addActionListener(new Reset());

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) {
                playZone[i][j].putClientProperty("INDEX", new Integer[]{i, j});
                playZone[i][j].addActionListener(new TratareButon());
            }


        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                add(playZone[i][j]);

        add(player1);
        add(player2);
        add(name2);
        add(name1);
        add(turnValue);
        add(win);
        add(turnLabel);
        add(symbol1);
        add(symbol2);
        add(reset);

    }

    public static void main(String[] args) {
        new XOGame();
    }

    class Reset implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            XOGame.this.setVisible(false);
            new XOGame();
        }
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int winner = 0;
            if (XOGame.this.name1.getText().compareTo("") == 0 || XOGame.this.name2.getText().compareTo("") == 0)
                showMessageDialog(null, "Name your players first");
            else {
                JButton b = (JButton) e.getSource();
                Integer[] index = (Integer[]) b.getClientProperty("INDEX");
                if (turn % 2 == 0) {
                    playZone[index[0]][index[1]].setText("X");
                    playZone[index[0]][index[1]].setEnabled(false);
                } else {
                    playZone[index[0]][index[1]].setText("0");
                    playZone[index[0]][index[1]].setEnabled(false);
                }
                turn++;
                turnValue.setText(turn + "");
                if (turn >= 5) {
                    winner = checkForWinner();
                }
                if (winner != 0) {

                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            playZone[i][j].setEnabled(false);
                }
                if (winner == 1) {
                    win.setText(XOGame.this.name1.getText().toUpperCase() + " WON THE GAME");
                    showMessageDialog(null, XOGame.this.name1.getText().toUpperCase() + " WON THE GAME");
                }
                if (winner == 2) {
                    win.setText(XOGame.this.name2.getText().toUpperCase() + " WON THE GAME");
                    showMessageDialog(null, XOGame.this.name2.getText().toUpperCase() + " WON THE GAME");
                }
                if (winner == 0 && turn == 9)
                {win.setText("DRAW");
                    showMessageDialog(null, "DRAW!");}


            }
        }
    }

    public int checkForWinner() {
        for (int i = 0; i < 3; i++)
            if (playZone[i][0].getText().compareTo(playZone[i][1].getText()) == 0 && playZone[i][2].getText().compareTo(playZone[i][1].getText()) == 0) {
                if (playZone[i][0].getText().compareTo("X") == 0)
                    return 1;
                else if (playZone[i][0].getText().compareTo("0") == 0)
                    return 2;
            }
        for (int i = 0; i < 3; i++)
            if (playZone[0][i].getText().compareTo(playZone[1][i].getText()) == 0 && playZone[1][i].getText().compareTo(playZone[2][i].getText()) == 0) {
                if (playZone[0][i].getText().compareTo("X") == 0)
                    return 1;
                else if (playZone[0][i].getText().compareTo("0") == 0)
                    return 2;
            }
        if (playZone[0][0].getText().compareTo(playZone[1][1].getText()) == 0 && playZone[1][1].getText().compareTo(playZone[2][2].getText()) == 0) {
            if (playZone[0][0].getText().compareTo("X") == 0)
                return 1;
            else if (playZone[0][0].getText().compareTo("0") == 0)
                return 2;
        }
        if (playZone[2][0].getText().compareTo(playZone[1][1].getText()) == 0 && playZone[1][1].getText().compareTo(playZone[0][2].getText()) == 0) {
            if (playZone[2][0].getText().compareTo("X") == 0)
                return 1;
            else if (playZone[2][0].getText().compareTo("0") == 0)
                return 2;
        }
        return 0;
    }


}
