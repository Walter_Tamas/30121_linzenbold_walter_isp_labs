package isp.linzenboldwalter.lab9.Exercise2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class CounterAndButton extends JFrame{

    JTextField value;
    JButton increment;

    CounterAndButton(){

        setTitle("Exercise 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=150;int height = 20;


        value = new JTextField();
        value.setBounds(10,50,width, height);
        value.setText("0");
        increment = new JButton("Increment");
        increment.setBounds(10,100,width, height);

        increment.addActionListener(new TratareButon());



      add(value);add(increment);

    }

    public static void main(String[] args) {
        new CounterAndButton();
    }

    class TratareButon implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            int number=Integer.parseInt(CounterAndButton.this.value.getText());
            number++;
            value.setText(number+"");

        }
    }
}