package isp.linzenboldwalter.lab8.Exercise4;

class TemperatureEvent extends Event {

    private int vlaue;

    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    int getVlaue() {
        return vlaue;
    }

    @Override
    public String toString() {
        if(getVlaue()>23)
        return "TemperatureEvent " + "value=" + vlaue +" "+ new CoolingUnit().toString();
        else
            return "TemperatureEvent " + "value=" + vlaue +" "+new HeatingUnit().toString();
    }

}