package isp.linzenboldwalter.lab8.Exercise4;

class FireEvent extends Event {

    private boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;



    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {

        if(isSmoke()==true) {
            return "FireEvent" + "smoke=" + smoke +" "+new Alarm().toString()+ " " +new GsmUnit().toString();
        }
        else
            return "FireEvent{" + "smoke=" + smoke +'}';
    }

}