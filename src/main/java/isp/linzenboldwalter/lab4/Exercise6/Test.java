package isp.linzenboldwalter.lab4.Exercise6;

public class Test {
    public static void main(String[] args) {
        Shape s = new Shape("blue", true);
        System.out.println(s.toString());
        Circle c = new Circle(2, "red", false);
        System.out.println(c.toString());
        Rectangle r = new Rectangle(2, 4, "red", true);
        System.out.println(r.toString());
        Square square = new Square(2, "blue", true);
        System.out.println(square.toString());
    }
}
