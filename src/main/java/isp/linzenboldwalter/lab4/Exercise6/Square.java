package isp.linzenboldwalter.lab4.Exercise6;

public class Square extends Rectangle {

    public Square() {

    }

    public Square(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide() {
        return this.getLength();
    }

    public void setSide(double side) {
        this.setWidth(side);
        this.setLength(side);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "A Square with side=" + this.getSide() + " which is subclass of " + super.toString();
    }
}
