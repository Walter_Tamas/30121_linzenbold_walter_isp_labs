package isp.linzenboldwalter.lab4.Exercise5;


public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {

    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return height * Math.PI * this.getRadius() * this.getRadius();
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * Math.pow(this.getRadius(), 2) + 2 * Math.PI * this.getRadius() * height;
    }
}
