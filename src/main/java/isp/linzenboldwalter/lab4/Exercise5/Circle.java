package isp.linzenboldwalter.lab4.Exercise5;

public class Circle {

    private double radius = 1.0;
    private String color = "red";

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {

        return radius;
    }

    public double getArea() {

        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Radius: " + radius + " ,color: " + color;
    }
}
