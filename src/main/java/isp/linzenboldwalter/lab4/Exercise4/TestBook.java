package isp.linzenboldwalter.lab4.Exercise4;


public class TestBook {
    public static void main(String[] args) {
        Author[] author = {new Author("Sun Tzu", "email@yahoo.com", 'm'),
                new Author("John Boyne", "email2@yahoo.com", 'm')};
        Book book = new Book("Arta razboiului", author, 30, 10);
        Book book2 = new Book("Baiatul cu pijamale in dungi", author, 25.5, 0);
        System.out.println(book.toString());
        book.printAuthors();
        System.out.println(book2.toString());
        book2.printAuthors();
    }

}
