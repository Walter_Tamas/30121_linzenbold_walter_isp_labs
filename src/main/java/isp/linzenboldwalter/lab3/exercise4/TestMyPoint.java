package isp.linzenboldwalter.lab3.exercise4;



public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint point = new MyPoint(5, 5);
        System.out.println("(" + point.getX() + "," + point.getY() + ")");
        double dist = point.distance(6, 6);
        System.out.println("Distance to " + point.toString() + " is:" + dist);
        MyPoint point2 = new MyPoint();
        point2.setX(7);
        point2.setY(7);
        System.out.println(point2.toString());
        System.out.println("Distance to " + point2.toString() + " is:" + point.distance(point2));
    }


}
