package isp.linzenboldwalter.lab3.exercise1;

public class Robot {
    int x;

    Robot() {

        x = 1;
    }

    public void change(int k) {
        if (k >= 1)
            x += k;
    }

    @Override
    public String toString() {

        return "Robot position is:" + x;
    }
}
