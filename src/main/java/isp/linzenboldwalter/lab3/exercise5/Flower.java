package isp.linzenboldwalter.lab3.exercise5;

public class Flower {
    int petal;
    public static int number_flowers = 0;

    Flower() {
        System.out.println("Flower has been created!");
        number_flowers++;
    }

    Flower(int p) {
        petal = p;
        System.out.println("New flower has been created!");
        number_flowers++;
    }

    public static int numberOfFlowers() {
        return number_flowers;
    }


}
