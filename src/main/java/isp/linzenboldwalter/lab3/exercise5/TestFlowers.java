package isp.linzenboldwalter.lab3.exercise5;

public class TestFlowers {
    public static void main(String[] args) {
        Flower f1 = new Flower();
        Flower f2 = new Flower(4);
        Flower f3 = new Flower(2);
        Flower f4 = new Flower(), f5 = new Flower(6), f6;
        System.out.println("Number of flowers constructed is:" + Flower.numberOfFlowers());
    }


}
