package isp.linzenboldwalter.lab3.exercise2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle("blue", 2);
        System.out.println(c1.getRadius());
        System.out.println(c1.getArea());
        System.out.println(c1.getColor());
        System.out.println(c2.getRadius());
        System.out.println(c2.getArea());
        System.out.print(c2.getColor());

    }
}
