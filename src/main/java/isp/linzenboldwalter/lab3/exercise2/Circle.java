package isp.linzenboldwalter.lab3.exercise2;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    Circle() {

    }

    Circle(String color, double r) {
        this.color = color;
        this.radius = r;
    }

    public double getRadius() {

        return this.radius;
    }

    public double getArea() {

        return Math.PI * this.radius * this.radius;
    }

    public String getColor() {

        return this.color;
    }
}
