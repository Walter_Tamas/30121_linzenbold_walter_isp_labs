package isp.linzenboldwalter.lab3.exercise3;

import isp.linzenboldwalter.lab3.exercise3.Author;

public class TestAuthor {
    public static void main(String[] args) {
        Author aut = new Author("Andreea", "andreea@email.com", 'f');
        System.out.println(aut.toString());
        aut.setEmail("andreea.@email.ro");
        System.out.println(aut.getName() + " (" + aut.getGender() + ") " + " at " + aut.getEmail());
    }
}
