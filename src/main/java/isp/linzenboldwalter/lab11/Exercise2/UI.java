package isp.linzenboldwalter.lab11.Exercise2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class UI extends JFrame implements Observer {

    JLabel productLabel, availableProducts, quantityLabel, priceLabel;
    JTextField product, quantity, price;
    JTextArea productList;
    JButton addProduct, delete, changeQuantity, show;
    ArrayList<Product> list = new ArrayList<Product>();

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("1");
        for (Product index : list) {

            if (index.name.compareTo(((Product) o).getName()) != 0) {
                list.add((Product) o);
                showItems();
            }
        }
    }

    UI() {
        setTitle("Stock management application");
        setLocation(700, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 420);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        productLabel = new JLabel("Product name:");
        productLabel.setBounds(10, 20, 150, 20);
        product = new JTextField();
        product.setBounds(150, 20, 200, 20);
        quantityLabel = new JLabel("Quantity");
        quantityLabel.setBounds(10, 40, 150, 20);
        quantity = new JTextField();
        quantity.setBounds(150, 40, 200, 20);
        priceLabel = new JLabel("Price");
        priceLabel.setBounds(10, 60, 150, 20);
        price = new JTextField();
        price.setBounds(150, 60, 200, 20);
        availableProducts = new JLabel("Products");
        availableProducts.setBounds(10, 100, 150, 20);
        productList = new JTextArea();
        productList.setBounds(10, 120, 350, 130);
        addProduct = new JButton("Add Product");
        addProduct.setBounds(20, 250, 200, 20);
        delete = new JButton("Delete Product");
        delete.setBounds(20, 280, 200, 20);
        changeQuantity = new JButton("Change Available Quantity");
        changeQuantity.setBounds(20, 310, 200, 20);
        show = new JButton("Show products");
        show.setBounds(20, 340, 200, 20);

        addProduct.addActionListener(new AddProduct());
        delete.addActionListener(new Delete());
        changeQuantity.addActionListener(new Change());
        show.addActionListener(new ShowItems());


        add(priceLabel);
        add(price);
        add(quantity);
        add(quantityLabel);
        add(delete);
        add(show);
        add(changeQuantity);
        add(addProduct);
        add(availableProducts);
        add(productLabel);
        add(product);
        add(productList);
    }

    class AddProduct implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Product p = new Product(product.getText(), Integer.parseInt(price.getText()), Integer.parseInt(quantity.getText()));
            list.add(p);
            product.setText("");
            price.setText("");
            quantity.setText("");
        }
    }

    class Delete implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (Product index : list) {
                if (index.name.compareTo(product.getText()) == 0)
                    list.remove(index);
            }
            product.setText("");
        }
    }

    class Change implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (Product index : list) {
                if (index.name.compareTo(product.getText()) == 0)
                    index.quantity = Integer.parseInt(quantity.getText());
            }
        }
    }

    class ShowItems implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            productList.setText("");
            for (Product index : list) {
                productList.append(index.toString());
            }
        }
    }

    public void showItems() {
        productList.setText("");
        for (Product index : list) {
            productList.append(index.toString());
        }
    }


    public static void main(String[] args) {
        new UI();
    }
}
