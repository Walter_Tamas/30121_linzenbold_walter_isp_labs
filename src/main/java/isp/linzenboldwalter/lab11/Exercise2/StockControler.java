package isp.linzenboldwalter.lab11.Exercise2;

public class StockControler {
    Product p;
    UI view;
    public StockControler(Product p,UI view){
        p.addObserver(view);
        this.p=p;
        this.view=view;
    }

    public static void main(String[] args) {
        UI view=new UI();
        Product p=new Product();
        StockControler sControler=new StockControler(p,view);
    }
}
