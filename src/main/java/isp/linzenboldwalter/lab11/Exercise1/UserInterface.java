package isp.linzenboldwalter.lab11.Exercise1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class UserInterface extends JFrame implements Observer {
    JTextField text;
    JLabel label;
    JButton active, pause;
    static boolean state = false;
    boolean paused = false;
    Sensor sensor = new Sensor(this);


    UserInterface() {
        setTitle("Sensor Monitoring app");

        setLocation(700, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    void init() {
        this.setLayout(null);
        sensor.addObserver(this);
        label = new JLabel("Temperature:");
        label.setBounds(20, 20, 150, 20);
        text = new JTextField();
        text.setEditable(false);
        text.setBounds(180, 20, 50, 20);
        active = new JButton("Activate");
        active.setBounds(20, 60, 150, 20);
        pause = new JButton("Pause");
        pause.setBounds(20, 100, 150, 20);
        active.addActionListener(new Active());
        pause.addActionListener(new Pause());
        add(pause);
        add(active);
        add(label);
        add(text);
    }

    class Active implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (state) {
                state = false;

            } else {
                state = true;
                active.setText("Stop");
            }
        }
    }

    class Pause implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (paused) {
                paused = false;
                pause.setText("Pause");
            } else {
                pause.setText("Resume");
                paused = true;
            }
            sensor.setPause(paused);
        }
    }

    public void update(Observable o, Object arg) {
        String s = "" + ((Sensor) o).getTemperature();
        text.setText(s);
    }

    public static void main(String[] args) {
        UserInterface u = new UserInterface();
        while (state == false) {
            System.out.println(state);
        }
        u.sensor.start();

    }
}
