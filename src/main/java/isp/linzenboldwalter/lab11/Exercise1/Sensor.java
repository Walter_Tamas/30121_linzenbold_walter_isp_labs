package isp.linzenboldwalter.lab11.Exercise1;

import java.util.Observable;
import java.util.Random;

public class Sensor extends Observable implements Runnable {
    int ptemperature = 0, ctemperature;
    UserInterface uI;
    Thread t;

    Sensor(UserInterface uI) {
        this.uI = uI;
    }

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }

    public void run() {

        while (uI.state == true) {
            if (uI.paused == true) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            ctemperature = new Random().nextInt(100);

            if (ptemperature != ctemperature) {
                this.setChanged();
                System.out.println(ctemperature);
                this.notifyObservers();
            }
            try {
                Thread.sleep(1000);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    void setPause(boolean paused){
        synchronized (this){
            if(paused==true)
                paused=true;
            else {
                paused=false;
                notify();
            }
        }
    }

    public int getTemperature() {
        return ctemperature;
    }

}
