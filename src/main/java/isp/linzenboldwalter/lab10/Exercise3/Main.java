package isp.linzenboldwalter.lab10.Exercise3;

public class Main {
    public static void main(String[] args) {
        Counter c1=new Counter("counter1",0,100,null);
        Counter c2=new Counter("counter2",100,200,c1);

        c1.start();
        c2.start();
    }
}
