package isp.linzenboldwalter.lab10.Exercise3;

public class Counter extends Thread {

    int initialValue,finalValue;
    Thread t;
    Counter(String name, int value1,int value2,Thread thread) {
        super(name);
        this.initialValue = value1;
        this.finalValue=value2;
        this.t=thread;
    }

    public void run() {
        for (int i = initialValue; i <=finalValue; i++) {

            try {
                if(t!=null) t.join();
                System.out.println(getName() + " i = " + (i));
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}