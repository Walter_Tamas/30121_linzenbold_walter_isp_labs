package isp.linzenboldwalter.lab10.exercise6;

import isp.linzenboldwalter.lab9.Exercise4.XOGame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UI extends JFrame {

    boolean paused=false;
     public  JTextField text;
    JButton start,reset;
    Chronometer  ch=new Chronometer("Chrono");

   UI() {
        setTitle("Chronometer");
        setLocation(700, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }
    void init(){
       this.setLayout(null);
       text=new JTextField();
       text.setBounds(20,20,100,20);
       start=new JButton("Start/Pause");
       start.setBounds(20,100,150,40);
       reset=new JButton("Reset");
       reset.setBounds(20,160,150,40);

       reset.addActionListener(new Reset());
       start.addActionListener(new Start());
       add(text);
       add(start);
       add(reset);
    }

    class Reset implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           new UI();
        }
    }

    class Start implements ActionListener {
        public void actionPerformed(ActionEvent e) {
          if(paused==false)
          {ch.start();
          paused=true;
          text.setText(ch.seconds+"");}
          else
          {
              paused=false;
          ch.stop_count();
          }

        }
    }

    public static void main(String[] args) {
       new UI();
    }
}
