package isp.linzenboldwalter.lab10.exercise6;

public class Chronometer extends Thread{
    private long begin, end;
    public  int seconds=0;
    Chronometer(String name){
        super(name);
    }

    public void run()
    {
        try {
            Thread.sleep(1000);
            seconds++;
            System.out.println(seconds);
            }catch(Exception e){
                e.printStackTrace();
            }

        }

    public void start_count(){
        begin = System.currentTimeMillis();
    }

    public void stop_count(){
        end = System.currentTimeMillis();
    }

    public long getTime() {
        return end-begin;
    }

    public long getMilliseconds() {
        return end-begin;
    }

    public double getSeconds() {
        return (end - begin) / 1000.0;
    }

    public double getMinutes() {
        return (end - begin) / 60000.0;
    }

    public double getHours() {
        return (end - begin) / 3600000.0;
    }
}