package isp.linzenboldwalter.lab10.Exercise4;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class Robot extends Thread {

    private String name;
    Thread t;
    private int x;
    private int y;
    protected boolean isDestroyed = false;
    private static int counter = 0;



    private static ArrayList<Robot> robotArrayList = new ArrayList<Robot>();




    Robot(String name){
        this.name = name;
    }

    public void run() {
        while (counter < 10) {
            if (this.isDestroyed) {
                counter++;
                return;
            }
            try {
                Thread.sleep(100);
                moveRobot();

                if (!this.isDestroyed) {
                    System.out.println(name + " position:" + x + "," + y);
                }

                for (Robot index : robotArrayList) {

                    if (!index.isDestroyed)
                        checkPositions(this, index);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }



    public void moveRobot(){
        Random r1 = new Random();
        Random r2 = new Random();
        this.x = r1.nextInt(11);
        this.y = r2.nextInt(11);

    }

    public void checkPositions(Robot r1,Robot r2){
        if(r1.x==r2.x && r1.y==r2.y && !r1.name.equals(r2.name)){
            System.out.println(r1.name + " and " + r2.name + " are destroyed");

            r1.isDestroyed = true;
            r2.isDestroyed = true;





        }

    }

    public static void main(String[] args) {

        robotArrayList.add(new Robot("robot1"));
        robotArrayList.add(new Robot("robot2"));
        robotArrayList.add(new Robot("robot3"));
        robotArrayList.add(new Robot("robot4"));
        robotArrayList.add(new Robot("robot5"));
        robotArrayList.add(new Robot("robot6"));
        robotArrayList.add(new Robot("robot7"));
        robotArrayList.add(new Robot("robot8"));
        robotArrayList.add(new Robot("robot9"));
        robotArrayList.add(new Robot("robot10"));

        for(Robot index : robotArrayList){
            index.start();
        }


    }


}
