package isp.linzenboldwalter.lab7.Exercise3;

import java.io.IOException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);

            int option;
            String s = new String();
            do {
                System.out.println("1-File encryption\n2-File decryption\n3-Read name of the file that is going to be encrypted/decrypted\n4-Exit");
                option = scanner.nextInt();
                switch (option) {
                    case 1:
                        Encrypt.encrypt(s);
                        break;
                    case 2:
                        Decrypt.decrypt(s);
                        break;
                    case 3:
                        System.out.print("File name is:");
                        s = scanner.next();
                        break;
                }
            } while (option != 4);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}