package isp.linzenboldwalter.lab7.Exercise4;

import java.io.*;

class CarFactory{
    Car createCar(String model,int price){
        Car z = new Car(model,price);
        System.out.println(z+" is working");
        return z;
    }

    void hideCar(Car a, String storeRecipientName) throws IOException {
        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream(storeRecipientName));

        o.writeObject(a);
        System.out.println("Car details:"+a);
    }

    Car showCar(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(storeRecipientName));
        Car x = (Car)in.readObject();
        System.out.println("Car details:"+x);
        return x;
    }

}//.class