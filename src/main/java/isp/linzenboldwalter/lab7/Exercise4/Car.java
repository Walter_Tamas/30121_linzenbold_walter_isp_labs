package isp.linzenboldwalter.lab7.Exercise4;

import java.io.Serializable;

class Car implements Serializable {
    String model;
    int price;

    public Car(String s, int price) {
        this.model = s;
        this.price = price;
    }

    public String toString() {
        return "[car=" + model + ", price=" +price + "]";
    }
}