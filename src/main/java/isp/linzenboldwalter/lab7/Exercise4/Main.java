package isp.linzenboldwalter.lab7.Exercise4;

public class Main {
    public static void main(String[] args) throws Exception{
        CarFactory f = new CarFactory();

        Car a = f.createCar("Dacia",10000);
        Car b = f.createCar("Toyota",30000);
        System.out.println();
        f.hideCar(a,"car1.dat");
        f.hideCar(b,"car1.dat");
        System.out.println();
        Car x = f.showCar("car1.dat");
        Car y = f.showCar("car1.dat");
        System.out.println(x);
        System.out.println(y);
    }
}//.class