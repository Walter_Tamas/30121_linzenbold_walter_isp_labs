package isp.linzenboldwalter.lab7.Exercise2;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        try{
            int number=Count.count();
            System.out.println("Number of times our character appears in file data.txt is:"+number);
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
