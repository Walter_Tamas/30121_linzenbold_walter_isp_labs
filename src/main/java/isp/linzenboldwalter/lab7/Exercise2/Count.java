package isp.linzenboldwalter.lab7.Exercise2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Count {
    public static int count() throws IOException {
        int number = 0;
        char fixedChar = 'e';
        // 1. Reading input by lines:
        BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\MSI_Gamer\\Desktop\\ISP LABS\\30121_linzenbold_walter_isp_labs\\src\\main\\java\\isp\\linzenboldwalter\\lab7\\Exercise2\\data.txt"));
        String s = new String();
        while ((s = in.readLine()) != null)
            for (int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (ch == fixedChar)
                    number++;
            }
        in.close();
        return number;
    }
}
