package isp.linzenboldwalter.lab6.Exercise3;

import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;


public class Bank {

    BankAccount bankAccount;
    TreeSet<BankAccount> treeSet = new TreeSet<BankAccount>();
    TreeSet<BankAccount> treeSet2 = new TreeSet<BankAccount>(new sort());

    public void addAccount(String owner, double balance) {
        bankAccount = new BankAccount(owner, balance);
        treeSet.add(bankAccount);
        treeSet2.add(bankAccount);
    }

    public void printAccounts() {

        for (BankAccount index : treeSet) {
            System.out.println(index.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount index : treeSet) {
            if (index.getBalance() >= minBalance && index.getBalance() <= maxBalance) {
                System.out.println(index.toString());
            }
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount index : treeSet) {
            if (index.getOwner().equals(owner)) {
                return index;
            }

        }
        return null;
    }

    public void getAllAccounts() {

        for (BankAccount index : treeSet2) {
            System.out.println(index);
        }

    }


    class sort implements Comparator<BankAccount> {

        @Override
        public int compare(BankAccount bankAccount1, BankAccount bankAccount2) {
            return bankAccount1.getOwner().compareTo(bankAccount2.getOwner());
        }
    }
}
