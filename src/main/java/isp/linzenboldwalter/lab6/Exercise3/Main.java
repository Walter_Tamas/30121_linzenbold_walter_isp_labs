package isp.linzenboldwalter.lab6.Exercise3;

public class Main {

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Walter", 300);
        bank.addAccount("Florin", 1200);
        bank.addAccount("Tamas", 200);
        bank.addAccount("Ana", 1000);
        bank.addAccount("Maria", 50);
        bank.addAccount("Georgiana", 1);
        bank.printAccounts();
        System.out.println();
        bank.printAccounts(100, 300);
        System.out.println();
        System.out.println(bank.getAccount("Tamas"));
        System.out.println();
        bank.getAllAccounts();
    }
}
