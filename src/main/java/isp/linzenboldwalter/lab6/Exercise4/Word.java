package isp.linzenboldwalter.lab6.Exercise4;

public class Word {
    private String name;

    public Word(String name) {
        this.name = name;
    }

    public Word() {

    }

    public String getWord() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Word))
            return false;
        Word x = (Word)obj;
        return name.equals(x.name);
    }

    public int hashCode() {
        return (int)(name.length()*1000);
    }


}
