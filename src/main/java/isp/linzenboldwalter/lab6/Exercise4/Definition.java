package isp.linzenboldwalter.lab6.Exercise4;

public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }

    public Definition() {

    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.getDescription();
    }
}

