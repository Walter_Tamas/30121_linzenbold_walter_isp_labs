package isp.linzenboldwalter.lab6.Exercise4;

import java.util.HashMap;

public class Dictionary {
    private  HashMap<Word, Definition> dictionary ;

    public Dictionary(HashMap<Word,Definition> dictionary) {
        this.dictionary=dictionary;
    }

    public void addWord(Word word, Definition definition) {

        dictionary.put(word, definition);

    }

    public Definition getDefinition(Word word) {

        return dictionary.get(word);
    }

    public void getAllWords() {
        for (Word index : dictionary.keySet()) {
            System.out.println(index);
        }
    }

    public void getAllDefinitions() {
        for (Definition index : dictionary.values()) {
            System.out.println(index.toString());
        }
    }


    public void listDictionary() {
        for (Word index : dictionary.keySet()) {
            System.out.println(index + " " + dictionary.get(index));
        }
    }

}
