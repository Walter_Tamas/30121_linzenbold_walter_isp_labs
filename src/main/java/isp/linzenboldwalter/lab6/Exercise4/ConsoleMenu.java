package isp.linzenboldwalter.lab6.Exercise4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ConsoleMenu {

    public static void main(String[] args) throws IOException {

        HashMap<Word,Definition> hashMap=new HashMap<Word,Definition>();
        Dictionary dictionary = new Dictionary(hashMap);
        char optiune;
        String linie, explicatie;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("c - Cauta cuvant");
            System.out.println("l - Listeaza dictionar");
            System.out.println("e - Iesire");

            linie = fluxIn.readLine();
            optiune = linie.charAt(0);

            switch (optiune) {
                case 'a':
                case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Introduceti definitia:");
                        explicatie = fluxIn.readLine();
                        dictionary.addWord(new Word(linie), new Definition(explicatie));
                    }
                    break;
                case 'c':
                case 'C':
                    System.out.println("Cuvant cautat:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {

                        System.out.println("Explicatie: " + dictionary.getDefinition(new Word(linie)));

                    }
                    break;
                case 'l':
                case 'L':
                    System.out.println("Afiseaza:");
                    dictionary.listDictionary();
                    break;

            }
        } while (optiune != 'e' && optiune != 'E');
        System.out.println("Program terminat!");
    }
}


