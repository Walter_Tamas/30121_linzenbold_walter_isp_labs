package isp.linzenboldwalter.lab6.Exercise1;

public class Main {
    public static void main(String[] args) {
        BankAccount b=new BankAccount("Walter",300);
        BankAccount b2=new BankAccount("Tamas",200);
        BankAccount b3=new BankAccount("Walter",300);
        System.out.println(b.equals(b2));
        System.out.println(b.equals(b3));
        System.out.println(b.hashCode());
        System.out.println(b2.hashCode());
        System.out.println(b3.hashCode());
    }
}
