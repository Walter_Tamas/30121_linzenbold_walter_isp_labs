package isp.linzenboldwalter.lab6.Exercise1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw(double amount){
        balance=balance-amount;
    }

    public void deposit(double amount){
        balance=balance+amount;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o){
        if(o==this)
            return true;
        if(o==null)
            return false;
        BankAccount b=(BankAccount) o;
        if(this.owner==b.owner&&this.balance== b.balance)
            return true;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

}
