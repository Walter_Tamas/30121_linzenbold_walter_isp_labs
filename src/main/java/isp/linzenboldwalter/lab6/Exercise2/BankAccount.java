package isp.linzenboldwalter.lab6.Exercise2;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount() {
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void withdraw(double amount) {
        balance = balance - amount;
    }

    public void deposit(double amount) {
        balance = balance + amount;
    }

    @Override
    public String toString() {
        return "Owner is: " + owner + " and balance is: " + balance;
    }

}
