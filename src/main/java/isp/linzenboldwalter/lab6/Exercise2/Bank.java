package isp.linzenboldwalter.lab6.Exercise2;

import java.util.ArrayList;

public class Bank {

    BankAccount account;
    ArrayList<BankAccount> bank = new ArrayList<BankAccount>();


    public void addAccount(String owner, double balance) {
         this.account = new BankAccount(owner, balance);
        bank.add(account);
    }

    public void printAccounts() {
        int i, j;
        for (i = 0; i < bank.size() - 1; i++)
            for (j = i + 1; j < bank.size(); j++)
                if (bank.get(i).getBalance() > bank.get(j).getBalance()) {
                    BankAccount aux ;
                    aux = bank.get(i);
                    bank.set(i, bank.get(j));
                    bank.set(j, aux);
                }
        for (i = 0; i < bank.size(); i++)
            System.out.println("Bank account nr." + (i + 1) + " : " + bank.get(i).toString());
    }

    public void printAccounts(double minBalance, double maxBalance) {
        int i = 0;
        for (i = 0; i < bank.size(); i++)
            if (bank.get(i).getBalance() >= minBalance && bank.get(i).getBalance() <= maxBalance)
                System.out.println("Bank account nr." + (i + 1) + " : " + bank.get(i).toString());
    }

    public void printAlphabetically(){
        int i, j;
        for (i = 0; i < bank.size() - 1; i++)
            for (j = i + 1; j < bank.size(); j++)
                if (bank.get(i).getOwner().compareTo(bank.get(j).getOwner())>0) {
                    BankAccount aux = new BankAccount();
                    aux = bank.get(i);
                    bank.set(i, bank.get(j));
                    bank.set(j, aux);
                }
        for (i = 0; i < bank.size(); i++)
            System.out.println("Bank account nr." + (i + 1) + " : " + bank.get(i).toString());
    }


    public BankAccount getAccount(String owner) {
        int i;
        for (i = 0; i < bank.size(); i++)
            if (bank.get(i).getOwner() == owner)
                return bank.get(i);

        return null;
    }
}
