package isp.linzenboldwalter.lab6.Exercise2;


public class Main {
    public static void main(String[] args) {
        Bank b=new Bank();
        b.addAccount("Walter",200);
        b.addAccount("Tamas",300);
        b.addAccount("Robert",100);
        b.addAccount("Florin",550);
        b.addAccount("Ana",1000);
        b.printAccounts(0,500);
        System.out.println();
        b.printAccounts();
        System.out.println();
        b.printAlphabetically();
    }
}
