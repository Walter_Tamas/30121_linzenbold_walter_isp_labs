package isp.linzenboldwalter.lab5.Exercise1;

public class Main {
    public static void main(String[] args) {
        Shape[] s = {new Circle(2),
                    new Rectangle(2, 4),
                    new Square(2, "blue", true)};
        /// Shape s=new Shape();   -nu se pot instantia astfel
        System.out.println(s[0].toString()+" ,area: " + s[0].getArea() + " and perimeter: " + s[0].getPerimeter());
        System.out.println(s[1].toString()+" ,area: "+s[1].getArea()+" and perimeter: "+s[1].getPerimeter());
        System.out.println(s[2].toString()+" ,area: "+s[2].getArea()+" and perimeter: "+s[2].getPerimeter());

    }
}
