package isp.linzenboldwalter.lab5.Exercise5;

import java.util.Random;

public class TemperatureSensor extends Sensor { ////inheritance

    public int readValue() {
        Random random = new Random();
        int value = random.nextInt(100);
        return value;
    }

}
