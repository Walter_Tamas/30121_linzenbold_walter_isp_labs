package isp.linzenboldwalter.lab5.Exercise5;

public class Controller {
    private TemperatureSensor tempSensor = new TemperatureSensor();
    private LightSensor lightSensor = new LightSensor();

    public void control() {

        int i = 0;
        while (i < 20) {
            try {
                Thread.sleep(1000);
                System.out.println("Temperature sensor value=" + tempSensor.readValue() + " and light sensor value=" + lightSensor.readValue());
                i++;
            } catch (InterruptedException e) {
                System.out.println("got interrupted!");
            }
        }

    }

}
