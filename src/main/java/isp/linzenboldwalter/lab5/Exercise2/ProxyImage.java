package isp.linzenboldwalter.lab5.Exercise2;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;

    public ProxyImage(String fileName,Image image) {
        this.fileName = fileName;
        this.image=image;
    }

    @Override
    public void display() {
        image.display();
    }
}

