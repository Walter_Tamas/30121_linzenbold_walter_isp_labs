package isp.linzenboldwalter.lab5.Exercise2;

public class Main {
    public static void main(String[] args) {
        RealImage realImage = new RealImage("image1");
        ProxyImage proxyImage = new ProxyImage("image", realImage);
        proxyImage.display();
        System.out.println();
        RotatedImage rotatedImage = new RotatedImage("image2");
        ProxyImage proxyImage1 = new ProxyImage("image", rotatedImage);
        proxyImage1.display();




    }
}
