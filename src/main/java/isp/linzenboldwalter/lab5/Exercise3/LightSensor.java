package isp.linzenboldwalter.lab5.Exercise3;


import java.util.Random;

public class LightSensor extends Sensor {   ////inheritance

    public int readValue() {
        Random r = new Random();
        int value = r.nextInt(100);
        return value;
    }
}
