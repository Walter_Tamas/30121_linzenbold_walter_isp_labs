package isp.linzenboldwalter.lab5.Exercise3;

public class Main {
    public static void main(String[] args) {
        TemperatureSensor temp=new TemperatureSensor();
        LightSensor light=new LightSensor();
        Controller c=new Controller(light,temp);
        c.control();
    }
}
