package isp.linzenboldwalter.lab5.Exercise3;


public class Controller {

    LightSensor lightSensor;
    TemperatureSensor temperatureSensor;

    Controller(LightSensor l,TemperatureSensor t){
        this.lightSensor=l;
        this.temperatureSensor=t;
    }
    public void control() {

        int i = 0;
        while (i < 20) {
            try {
                Thread.sleep(1000);
                System.out.println("Temperature sensor value=" + temperatureSensor.readValue() + " and light sensor value=" + lightSensor.readValue());
                i++;
            } catch (InterruptedException e) {
                System.out.println("got interrupted!");
            }
        }

    }

}
