package isp.linzenboldwalter.lab5.Exercise4;


public class Controller {

    private static Controller controller;

    private Controller() {
    }

    public static Controller getController() {
            if (controller == null) {
                controller = new Controller();
            }

        return controller;
    }

    public void control(TemperatureSensor tempSensor, LightSensor lightSensor) {

        int i = 0;
        while (i < 20) {
            try {
                Thread.sleep(1000);
                System.out.println("Temperature sensor value=" + tempSensor.readValue() + " and light sensor value=" + lightSensor.readValue());
                i++;
            } catch (InterruptedException e) {
                System.out.println("got interrupted!");
            }
        }

    }

}
