package isp.linzenboldwalter.lab2;

import java.util.Scanner;
import java.util.Scanner;

public class Exercitiul6 {
    public static void main(String[] args) {
        int n=read();
        System.out.println("N! with non recursive method:"+non_recursive(n));
        System.out.println("N! with recursive method:"+recursive(n));
    }
    public static int read() {
        Scanner scanner=new Scanner(System.in);
        System.out.print("N=");
        int n=scanner.nextInt();
        return n;
    }
    public static int non_recursive(int n){
        int p=1;
        if(n==0)
            return 1;
        else
        for(int i=1;i<=n;i++)
            p=p*i;
        return p;
    }
    public static int recursive(int n){
        if(n==0)
            return 1;
        else
            return n*recursive(n-1);
    }
}
