package isp.linzenboldwalter.lab2;
import java.util.Random;

public class Exercitiul5 {
    public static int[] v;
    public static void main(String[] args) {
        generate();
        printarray(v);
        bubbleSort(v);
        printarray(v);
    }
    public static void generate() {
        v=new int[10];
        Random rand=new Random();
        for(int i=0;i<10;i++)
            v[i]= rand.nextInt(100);
    }
    public static void printarray(int a[]){
        int n = a.length;
        for (int i=0; i<n; ++i)
            System.out.print(a[i] + " ");
        System.out.println();
    }
    public static void bubbleSort(int a[]){
        int n = a.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (a[j] > a[j+1])
                {
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
    }
}
