package Colocviu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserInterface extends JFrame {
    JTextField nameField,departmentField;
    JLabel nameLabel,departmentLabel;
    JButton button,show;
    EmployeeService employees;


    UserInterface(EmployeeService e){
        this.setSize(500,200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        this.employees=e;
        this.setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        nameLabel=new JLabel("Full Name");
        nameLabel.setBounds(10,20,80,20);
        departmentLabel=new JLabel("Department");
        departmentLabel.setBounds(10,60,80,20);
        nameField=new JTextField();
        nameField.setBounds(100,20,300,20);
        departmentField=new JTextField();
        departmentField.setBounds(100,60,300,20);
        button=new JButton("Add");
        button.setBounds(150,100,100,20);
        show=new JButton("Show");
        show.setBounds(150,140,100,20);

        button.addActionListener(new Button());
        show.addActionListener(new Show());

        add(show);
        add(button);
        add(departmentField);
        add(nameField);
        add(nameLabel);
        add(departmentLabel);
    }

    class Button implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
                Employee employee=new Employee(nameField.getText(),departmentField.getText());
                employees.addEmployee(employee);
        }
    }

    class Show implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            employees.showEmployees();
        }
    }

}
