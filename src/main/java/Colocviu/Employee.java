package Colocviu;

public class Employee {
    protected int id;
    protected String fullName;
    protected String department;
    protected int startingYear;

    public Employee(String fullName,String department){
        this.fullName=fullName;
        this.department=department;
    }

    public Employee(int id,String fullName,String department,int startingYear){
        this.id=id;
        this.fullName=fullName;
        this.department=department;
        this.startingYear=startingYear;
    }

    public int getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDepartment() {
        return department;
    }

    public int getStartingYear() {
        return startingYear;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setStartingYear(int startingYear) {
        this.startingYear = startingYear;
    }
    public String toString(){
        return "id:"+id+" ,Name:"+fullName+" ,department:"+department+" ,starting year:"+startingYear;
    }
}
