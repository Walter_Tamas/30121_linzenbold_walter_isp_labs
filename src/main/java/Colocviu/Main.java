package Colocviu;

public class Main {

    public static void main(String[] args) {
            Employee e1=new Employee(1,"Ana","IT",2000);
            Employee e2=new Employee(2,"Walter","IT",1990);
            Employee e3=new Employee(3,"Florin","IT",2010);
            EmployeeService e=EmployeeService.getEmployeeService();

            //adding the employees
            e.addEmployee(e3);
            e.addEmployee(e1);
            e.addEmployee(e2);

            e.showEmployees();
            UserInterface userInterface=new UserInterface(e);
            //find and update
            e.findEmployeeAndUpdate(1,2001,"Tech");
            e.showEmployees();

            //filter
            e.employees=e.getEmployeeByStartingYear(2000);
            e.showEmployees();
    }
}
