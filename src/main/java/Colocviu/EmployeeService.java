package Colocviu;

import isp.linzenboldwalter.lab6.Exercise2.BankAccount;

import java.util.ArrayList;

public class EmployeeService {
    public ArrayList<Employee> employees = new ArrayList<Employee>();
    private static EmployeeService employeeService;

    private EmployeeService() {

    }

    public static EmployeeService getEmployeeService() {
        if (employeeService == null) {
            employeeService = new EmployeeService();
        }
        return employeeService;
    }


    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void findEmployeeAndUpdate(int id, int startingYear, String department) {
        for (Employee index : employees) {
            if (index.getId() == id) {
                index.setStartingYear(startingYear);
                index.setDepartment(department);
            }
        }
    }

    public ArrayList<Employee> getEmployeeByStartingYear(int startingYear) {
        int i, j;
        ArrayList<Employee> employees1 = new ArrayList<>();
        for (Employee index : employees)
            if (index.getStartingYear() >= startingYear)
                employees1.add(index);
        for (i = 0; i < employees1.size() - 1; i++)
            for (j = i + 1; j < employees1.size(); j++) {
                if (employees1.get(i).getStartingYear() > employees1.get(j).getStartingYear()) {
                    Employee aux;
                    aux = employees1.get(i);
                    employees1.set(i, employees1.get(j));
                    employees1.set(j, aux);
                }

            }
        return employees1;
    }

    public void showEmployees() {
        System.out.println();
        for (Employee index : employees)
            System.out.println(index);
    }


}
