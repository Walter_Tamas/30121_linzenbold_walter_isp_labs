package ExercitiiJava;
import java.util.Scanner;

 //Verificare numar palindrom

public class Palindrom {
    public static void main(String[] args) {
        int n=read();
        int pal=palindrom(n);
        verif(n,pal);
    }
    public static int read() {
        Scanner scann=new Scanner(System.in);
        System.out.print("Enter a number:");
        int n=scann.nextInt();
        return n;
    }
    public static int palindrom(int n){
        int aux=0;
        while(n>0) {
            aux=aux*10+n%10;
            n/=10;
        }
        return aux;
    }
    public static void verif(int n,int pal) {
        if(n==pal)
            System.out.println("Numarul este palindrom");
        else
            System.out.println("Numarul nu este palindrom");
    }
}
