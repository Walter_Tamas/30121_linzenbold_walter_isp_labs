package ExercitiiJava;
import java.util.Scanner;

//pbinfo- Genmat5
public class genmat5 {
    public static int[][] m;
    public static void main(String[] args) {
       Scanner scann=new Scanner(System.in);
       int cifre=scann.nextInt();
       int n=nr_cifre(cifre);
       generare(n,cifre);
       afisare(m);
    }
    public static int nr_cifre(int n) {
        int nr=0;
        while(n!=0) {
            nr++;n/=10;
        }
        return nr;
    }
    public static void afisare(int[][] matrix){
        int i,j;
        for(i=0;i<matrix.length;i++) {
            for(j=0;j<matrix[0].length;j++)
                System.out.print(matrix[i][j]+" ");
            System.out.print("\n");
        }
    }
    public static void generare(int n,int cifre) {
        m = new int[n][n];
        int i,j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++)
                m[j][i] = cifre % 10;
            cifre /= 10;
        }
    }
}
