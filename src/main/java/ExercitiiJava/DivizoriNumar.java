package ExercitiiJava;
import java.util.Scanner;
// Divizorii unui numar intreg pozitiv
public class DivizoriNumar {
    public static void main(String[] args) {
        int n=read();
        divisors(n);
    }
    public static int read() {
        Scanner scanner=new Scanner(System.in);
        System.out.print("Enter N=");
        int n=scanner.nextInt();
        return n;
    }
    public static void divisors(int n){
        int i;
        for(i=1;i<=n/2;i++)
            if(n%i==0)
                System.out.print(i+" ");
        System.out.print(n);
    }
}
