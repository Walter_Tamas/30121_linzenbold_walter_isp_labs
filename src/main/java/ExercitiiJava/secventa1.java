package ExercitiiJava;
import java.util.Scanner;

//pbinfo-secventa1 -Sa se verifice daca secventa y este o subsecventa a lui x
public class secventa1 {
    public static int[] x,y;
    public static void main(String[] args) {
        citire();
        int pos=subsecventa();
        if(pos==-1)
            System.out.println("NU");
        else
            System.out.println(pos+1);
    }
    public static void citire(){
        Scanner scann=new Scanner(System.in);
        int n=scann.nextInt();
        x=new int[n];
        for(int i=0;i<n;i++)
            x[i]=scann.nextInt();
        int m=scann.nextInt();
        y=new int[m];
        for(int i=0;i<m;i++)
            y[i]=scann.nextInt();
    }
    public static int subsecventa(){
        int i,j;
        for(i=0;i<x.length-y.length+1;i++)
        {
            int l=0;
            for(j=0;j<y.length;j++)
                if(x[j+i]==y[j])
                    l++;
             if(l==y.length)
                 return i;
        }
        return -1;
    }
}
