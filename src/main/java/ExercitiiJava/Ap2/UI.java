package ExercitiiJava.Ap2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class UI extends JFrame {

    JTextField path, text;
    JLabel pathLabel,textLabel;
    JButton execute;


    UI() {
        this.setTitle("Exercitiul 2");
        this.setSize(400, 400);
        setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        path = new JTextField();
        path.setBounds(60, 20, 300, 20);
        pathLabel=new JLabel("Path:");
        pathLabel.setBounds(10,20,50,20);
        text = new JTextField();
        text.setBounds(60, 60, 300, 20);
        textLabel=new JLabel("Text:");
        textLabel.setBounds(10,60,50,20);
        execute = new JButton("Write");
        execute.setBounds(110, 100, 100, 20);

        execute.addActionListener(new Button());

        add(path);add(pathLabel);add(textLabel);
        add(text);
        add(execute);

    }

    class Button implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (path.getText().compareTo("") != 0) {
                try {
                    PrintWriter out = new PrintWriter(path.getText());
                    out.print(text.getText());
                    //System.out.println(text.getText());
                    out.close();
                } catch (Exception err) {
                    System.out.println(err.getMessage());
                }
            }

        }
    }

    public static void main(String[] args) {
        new UI();
    }

}
