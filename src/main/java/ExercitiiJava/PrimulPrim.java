package ExercitiiJava;
import java.util.Random;

//Gaseste primul numar stric mai mare decat un numar generat aleatoriu

public class PrimulPrim {

    public static void main(String[] args) {
        int n=generate();
        System.out.println("First prime number is:"+firstPrime(n));

    }
    public static int generate(){
        Random rnd=new Random();
        int nr=rnd.nextInt(1000);
        System.out.println("Our number is:"+nr);
        return nr;
    }
    public static int firstPrime(int n) {
       int i;
       for(i=n+1;prime(i)==0;i++) {
           continue;
       }
       return i;
    }
    public static int prime(int nr) {
        if(nr<2)
            return 0;
        if(nr==2||nr==3)
            return 1;
        else if(nr%2==0)
            return 0;
        else
            for(int i=3;i*i<=nr;i+=2)
                if(nr%i==0)
                    return 0;
        return 1;
    }
}
