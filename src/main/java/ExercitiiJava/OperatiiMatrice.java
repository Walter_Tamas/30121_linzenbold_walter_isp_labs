package ExercitiiJava;
import java.util.Scanner;

public class OperatiiMatrice {
    public static int[][] a;
    public static void main(String[] args) {
        citire();
        afisare(a);
        Sum_row(a);
    }
    public static void citire(){
        int i,j,n,m;
        Scanner scanner =new Scanner(System.in);
        System.out.print("Number of rows:");
        n=scanner.nextInt();
        System.out.print("Number of columns:");
        m=scanner.nextInt();
        a=new int[n][m];
        for(i=0;i<n;i++)
            for(j=0;j<m;j++) {
                System.out.print("a["+i+"]["+j+"]=");
                a[i][j]=scanner.nextInt();
            }
    }
    public static void afisare(int[][] matrix){
        int i,j;
        for(i=0;i<matrix.length;i++) {
            for(j=0;j<matrix[0].length;j++)
                System.out.print(matrix[i][j]+" ");
            System.out.print("\n");
        }
    }
    public static void Sum_row(int[][] matrix){
        int i,j,s;
        for(i=0;i<matrix.length;i++) {
            s=0;
            for(j=0;j<matrix[0].length;j++)
               s+=matrix[i][j];
            System.out.println("Sum of row nr."+i+" ="+s);
        }
    }
}
